#!/usr/bin/python3

import sys
import subprocess as sp

def make_ascii(image_data, width=50):
  result = sp.run(
    ['jp2a', '--colors', '--width=' + str(width), '/dev/stdin'],
    input=image_data,
    stdout=sp.PIPE
  )
  return result.stdout

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print('Usage: {} "filename"')
    exit(0)

  image_filename = sys.argv[1]
  image_data = None
  with open(image_filename, 'rb') as image_file:
    image_data = image_file.read()
  print(make_ascii(image_data))