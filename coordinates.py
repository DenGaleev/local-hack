#!/usr/bin/python3

import requests
import sys

def get_coordinates(location):
  url = 'https://geocoder.api.here.com/6.2/geocode.json'
  data = {
    'searchtext': location,
    'app_id': 'dmZLK63yKgb0T8YX31k7',
    'app_code': 'JwSiqzaKqdJ6o52xpoNqpg'
  }
  response = requests.get(url, data).json()

  tmp = response['Response']['View'][0]['Result'][0]['Location']['MapView']
  coords = (
    tmp['TopLeft']['Latitude'],
    tmp['TopLeft']['Longitude'],
    tmp['BottomRight']['Latitude'],
    tmp['BottomRight']['Longitude']
  )
  return coords

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print('Usage: {} "geo location"')
    exit(0)
  print(get_coordinates(sys.argv[1]))