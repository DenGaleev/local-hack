#!/usr/bin/python3

import sys

def is_color(tok):
  return ord(tok[0]) == 27

def put_labels(bbox, labels, img):
  left   = bbox[1]
  right  = bbox[3]
  up     = bbox[0]
  down   = bbox[2]

  height = len(img)
  width = 0
  for i in img[0]:
    if not is_color(i):
      width += 1

  for label in labels:
    name = label[0]
    y = label[1]
    x = label[2]

    tx = int((x - left) / (right - left) * width)
    ty = height - int((y - down) / (up - down) * height)

    if (ty >= height):
      ty = height - 1

    curx = 0
    sym_placed = 0

    for j in range(len(img[ty])):
      if is_color(img[ty][j]):
        continue
      curx += 1

      if curx >= tx and sym_placed < len(name):
        img[ty][j] = '\x1b[30m\x1b[47m' + name[sym_placed] + '\x1b[0m'
        sym_placed += 1

def parse_ascii(ascii):
  img = ascii.decode('utf-8').split('\n')

  imgtok = []

  for i in img:
    s = i.strip()
    tokens = []
    curcolor = False
    lasttok = ''
    for c in s:
      if ord(c) == 27:
        if len(lasttok) > 0:
          tokens.append(lasttok)
          lasttok = ''
        curcolor = True
        lasttok += c
      elif curcolor and c == 'm':
        lasttok += c
        tokens.append(lasttok)
        lasttok = ''
        curcolor = False
      else:
        lasttok += c
        if len(lasttok) == 1:
          tokens.append(lasttok)
          lasttok = ''
    if len(lasttok) > 0:
      tokens.append(lasttok)
    imgtok.append(tokens)

  return imgtok

def print_ascii(img, fout):
  for line in img:
    print(''.join(line), file=fout)