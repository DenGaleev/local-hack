#!/usr/bin/python3

import requests
import sys

def get_labels(bbox):
  url = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json'

  slat = (bbox[0] + bbox[2]) / 2
  slong = (bbox[1] + bbox[3]) / 2
  radius = 50000

  data = {
    'app_id': 'dmZLK63yKgb0T8YX31k7',
    'app_code': 'JwSiqzaKqdJ6o52xpoNqpg',
    'bbox': ','.join(map(str, bbox[:2])) + ';' + ','.join(map(str, bbox[2:])),
    'mode': 'retrieveAreas',
    'gen': 9,
    'maxresults': 100
  }
  res = requests.get(url, data).json()
  labels = []
  for view in res['Response']['View']:
    for result in view['Result']:
      loc = result['Location']
      clat = (loc['MapView']['TopLeft']['Latitude'] + loc['MapView']['BottomRight']['Latitude']) / 2
      clong = (loc['MapView']['TopLeft']['Longitude'] + loc['MapView']['BottomRight']['Longitude']) / 2

      try:
        labels.append((loc['Name'], clat, clong))
      except:
        try:
          labels.append((loc['Address']['District'], clat, clong))
        except:
          pass
  return labels

if __name__ == '__main__':
  if len(sys.argv) != 5:
    print('Usage: {} lat1 long1 lat2 long2')
    exit(0)
  print(get_labels(tuple(map(float, sys.argv[1:]))))
