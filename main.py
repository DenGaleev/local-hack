#!/usr/bin/python3

import ascii
import argparse
import coordinates
import mapview
import insert_text
import landmarks
import sys

def make_ascii_with_labels(location, width=50):
  coords = coordinates.get_coordinates(location)
  image = mapview.get_image(location)
  img_ascii = ascii.make_ascii(image, width)

  labels = landmarks.get_labels(coords)

  img_tokens = insert_text.parse_ascii(img_ascii)
  insert_text.put_labels(coords, labels, img_tokens)

  return img_tokens

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='')
  parser.add_argument('location', type=str, help='location name')
  parser.add_argument('width', type=int, nargs='?', help='image width')

  args = parser.parse_args()
  location = args.location
  width = args.width or 50

  insert_text.print_ascii(make_ascii_with_labels(location, width), sys.stdout)