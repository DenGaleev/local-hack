#!/usr/bin/python3

import requests
import sys
import coordinates

def get_image(location):
  coords = coordinates.get_coordinates(location)

  url = 'https://image.maps.api.here.com/mia/1.6/mapview'
  data = {
    'bbox': ','.join(map(str, coords)),
    'app_id': 'dmZLK63yKgb0T8YX31k7',
    'app_code': 'JwSiqzaKqdJ6o52xpoNqpg',
    'h': 1000,
    'w': 1000
  }
  response = requests.get(url, data)

  return response.content

if __name__ == '__main__':
  if len(sys.argv) != 3:
    print('Usage: {} "geo location" out.jpeg')
    exit(0)
  fout = open(sys.argv[2], 'wb')
  fout.write(get_image(sys.argv[1]))
  fout.close()